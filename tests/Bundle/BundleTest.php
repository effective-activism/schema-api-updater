<?php declare(strict_types=1);

namespace EffectiveActivism\SchemaApiUpdater\Tests\Bundle;

use EffectiveActivism\SchemaApiUpdater\EffectiveActivismSchemaApiUpdaterBundle;
use EffectiveActivism\SchemaApiUpdater\DependencyInjection\SchemaApiUpdaterExtension;
use EffectiveActivism\SchemaApiUpdater\Service\Updater;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class BundleTest extends KernelTestCase
{
    public function testBundle()
    {
        $bundle = new EffectiveActivismSchemaApiUpdaterBundle();
        $this->assertInstanceOf(SchemaApiUpdaterExtension::class, $bundle->getContainerExtension());
    }

    public function testExtension()
    {
        $containerBuilderStub = $this->createMock(ContainerBuilder::class);
        $definition = new Definition(Updater::class, []);
        $containerBuilderStub->method('getDefinition')->willReturn($definition);
        $extension = new SchemaApiUpdaterExtension();
        $extension->prepend($containerBuilderStub);
        $extension->load([
            [
                'shacl_file' => 'test',
            ],
        ], $containerBuilderStub);
        $this->assertEquals('schema_api_updater', $extension->getAlias());
    }
}
