<?php

namespace EffectiveActivism\SchemaApiUpdater\Tests\Environment;

use EffectiveActivism\SchemaApi\EffectiveActivismSchemaApiBundle;
use EffectiveActivism\SparQlClient\EffectiveActivismSparQlClientBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel;

class TestKernel extends Kernel
{
    public function registerBundles()
    {
        return [
            new FrameworkBundle(),
            new EffectiveActivismSparQlClientBundle(),
            new EffectiveActivismSchemaApiBundle(),
            new TestBundle(),
        ];
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(function (ContainerBuilder $container) {
            $container->loadFromExtension('framework', [
                'secret' => 'test',
                'session' => [
                    'enabled' => true,
                    'storage_id' => 'session.storage.mock_file',
                ],
                'router' => [
                    'resource' => __DIR__ . '/config/routes.yml',
                    'utf8' => true,
                ],
                'test' => true,
            ]);
            $container->loadFromExtension('sparql_client', [
                'sparql_endpoint' => 'http://test-sparql-endpoint:9999/blazegraph/sparql',
                'namespaces' => [
                    'schema' => 'https://schema.org/',
                ],
            ]);
            $container->loadFromExtension('schema_api', [
                'namespaces' => [
                    'schema' => 'https://schema.org/',
                ],
            ]);
            $container->loadFromExtension('schema_api_updater', [
                'shacl_file' => sys_get_temp_dir() . '/schema-api-updater-shacl.ttl',
            ]);
        });
    }
}
