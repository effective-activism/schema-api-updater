<?php

namespace EffectiveActivism\SchemaApiUpdater\Tests\Service;

use EffectiveActivism\SchemaApiUpdater\Service\Updater;
use EffectiveActivism\SchemaApiUpdater\Tests\Environment\TestKernel;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class UpdaterTest extends KernelTestCase
{
    public static function getKernelClass(): string
    {
        return TestKernel::class;
    }

    /**
     * @covers \EffectiveActivism\SchemaApiUpdater\Queue\Handler
     * @covers \EffectiveActivism\SchemaApiUpdater\Queue\Item
     * @covers \EffectiveActivism\SchemaApiUpdater\Service\Updater
     */
    public function testEndpointGetSchema()
    {
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-foo-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-some-property-1-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-some-property-2-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-bar-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-some-property-1-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-some-property-2-types.xml')),
        ]);
        $kernel = new TestKernel('test', true);
        $kernel->boot();
        $kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        /** @var Updater $updaterService */
        $updaterService = $kernel->getContainer()->get(Updater::class);
        // Create fake previous run.
        touch(sys_get_temp_dir() . '/schema-api-updater-shacl.ttl');
        /** @var resource $handle */
        $updaterService->run();
        $shacl_content = file_get_contents(sys_get_temp_dir() . '/schema-api-updater-shacl.ttl');
        unlink(sys_get_temp_dir() . '/schema-api-updater-shacl.ttl');
        $this->assertEquals(file_get_contents(__DIR__.'/../fixtures/shacl.txt'), $shacl_content);
    }
}
