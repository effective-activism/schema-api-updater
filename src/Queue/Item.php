<?php declare(strict_types=1);

namespace EffectiveActivism\SchemaApiUpdater\Queue;

class Item
{
    protected array $set;

    public function __construct(array $set)
    {
        $this->set = $set;
    }

    public function getSet(): array
    {
        return $this->set;
    }
}
