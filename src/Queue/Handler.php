<?php declare(strict_types=1);

namespace EffectiveActivism\SchemaApiUpdater\Queue;

use EffectiveActivism\SchemaApi\Constant;
use EffectiveActivism\SparQlClient\Client\SparQlClientInterface;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Constraint\FilterNotExists;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Optionally\Optionally;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Triple\Triple;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\Iri;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\PrefixedIri;
use EffectiveActivism\SparQlClient\Syntax\Term\Literal\PlainLiteral;
use EffectiveActivism\SparQlClient\Syntax\Term\Path\InversePath;
use EffectiveActivism\SparQlClient\Syntax\Term\Path\ZeroOrMorePath;
use EffectiveActivism\SparQlClient\Syntax\Term\TermInterface;
use EffectiveActivism\SparQlClient\Syntax\Term\Variable\Variable;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class Handler implements MessageHandlerInterface
{
    protected string $destination;

    protected SparQlClientInterface $sparQlClient;

    public function __construct(string $destination, SparQlClientInterface $sparQlClient)
    {
        $this->destination = $destination;
        $this->sparQlClient = $sparQlClient;
    }

    public function __invoke(Item $item)
    {
        $classVariable = new Variable('class');
        $labelVariable = new Variable('label');
        $commentVariable = new Variable('comment');
        $classSet = $item->getSet();
        /** @var TermInterface $classTerm */
        $classTerm = $classSet[$classVariable->getVariableName()];
        /** @var TermInterface $classLabelTerm */
        $classLabelTerm = $classSet[$labelVariable->getRawValue()];
        /** @var PlainLiteral $classCommentTerm */
        $classCommentTerm = isset($classSet[$commentVariable->getVariableName()]) ? $classSet[$commentVariable->getVariableName()] : new PlainLiteral('');
        // Get all properties.
        $properties = [];
        $propertyVariable = new Variable('property');
        $propertyLabelVariable = new Variable('label');
        $propertyClassesVariable = new Variable('classes');
        $propertyCommentVariable = new Variable('comment');
        $statement = $this->sparQlClient
            ->select([$propertyVariable, $commentVariable, $propertyLabelVariable])
            ->where([
                new Triple($classTerm, new ZeroOrMorePath(new PrefixedIri('rdfs', 'subClassOf')), $propertyClassesVariable),
                new Triple($propertyClassesVariable, new InversePath(new PrefixedIri('schema', 'domainIncludes')), $propertyVariable),
                new Triple($propertyVariable, new PrefixedIri('rdfs', 'label'), $propertyLabelVariable),
                new Optionally([new Triple($propertyVariable, new PrefixedIri('rdfs', 'comment'), $propertyCommentVariable)]),
                // Do not include pending properties.
                new FilterNotExists([
                    new Triple($propertyVariable, new PrefixedIri('schema', 'isPartOf'), new Iri('http://pending.schema.org'))
                ])
            ]);
        $propertySets = $this->sparQlClient->execute($statement);
        foreach ($propertySets as $propertySet) {
            /** @var Iri $propertyTerm */
            $propertyTerm = $propertySet[$propertyVariable->getVariableName()];
            /** @var PlainLiteral $propertyCommentTerm */
            $propertyCommentTerm = isset($propertySet[$commentVariable->getVariableName()]) ? $propertySet[$commentVariable->getVariableName()] : new PlainLiteral('');
            /** @var PlainLiteral $propertyLabelTerm */
            $propertyLabelTerm = $propertySet[$propertyLabelVariable->getVariableName()];
            // Get all types.
            $types = [];
            $typeVariable = new Variable('type');
            $statement = $this->sparQlClient
                ->select([$typeVariable])
                ->where([
                    new Triple($propertyTerm, new PrefixedIri('schema', 'rangeIncludes'), $typeVariable),
                    // Do not include pending types.
                    new FilterNotExists([
                        new Triple($typeVariable, new PrefixedIri('schema', 'isPartOf'), new Iri('http://pending.schema.org'))
                    ])
                ]);
            $typeSets = $this->sparQlClient->execute($statement);
            // Always include the Role class as type.
            $typeSets[] = [
                $typeVariable->getVariableName() => new Iri(sprintf('%sRole', Constant::BASE_ADDRESS)),
            ];
            foreach ($typeSets as $typeSet) {
                /** @var TermInterface $typeTerm */
                $typeTerm = $typeSet[$typeVariable->getVariableName()];
                $localPart = str_replace(array_values($this->sparQlClient->getNamespaces()), '', $typeTerm->getRawValue());
                $prefix = array_search(str_replace($localPart, '', $typeTerm->getRawValue()), $this->sparQlClient->getNamespaces());
                switch ($localPart) {
                    case 'Boolean':
                        $subType = 'xsd:boolean';
                        $kind = 'sh:datatype';
                        break;

                    case 'Date':
                        $subType = 'xsd:date';
                        $kind = 'sh:datatype';
                        break;

                    case 'DateTime':
                        $subType = 'xsd:dateTime';
                        $kind = 'sh:datatype';
                        break;

                    case 'Float':
                        $subType = 'xsd:float';
                        $kind = 'sh:datatype';
                        break;

                    case 'Integer':
                    case 'Number':
                        $subType = 'xsd:integer';
                        $kind = 'sh:datatype';
                        break;

                    case 'Text':
                    case 'Time':
                    case 'URL':
                        $subType = 'xsd:string';
                        $kind = 'sh:datatype';
                        break;

                    default:
                        $kind = 'sh:class';
                        $subType = sprintf('%s:%s', $prefix, $localPart);
                        break;
                }
                $types[$subType] = $kind;
            }
            $localPart = str_replace(array_values($this->sparQlClient->getNamespaces()), '', $propertyTerm->getRawValue());
            $prefix = array_search(str_replace($localPart, '', $propertyTerm->getRawValue()), $this->sparQlClient->getNamespaces());
            $properties[sprintf('%s:%s', $prefix, $localPart)] = [
                'comment' => $this->convertSchemaDescriptionToHtml($propertyCommentTerm->typeCoercedSerialize(new PrefixedIri('rdf', 'HTML'))),
                'label' => $propertyLabelTerm->getRawValue(),
                'types' => $types,
            ];
        }
        $localPart = str_replace(array_values($this->sparQlClient->getNamespaces()), '', $classTerm->getRawValue());
        $prefix = array_search(str_replace($localPart, '', $classTerm->getRawValue()), $this->sparQlClient->getNamespaces());
        $class = sprintf('%s:%s', $prefix, $localPart);
        // Write to file.
        $output = '';
        $output .= sprintf("%s\n", $class);
        $output .= "  a rdfs:Class ;\n";
        $output .= "  a sh:NodeShape ;\n";
        $output .= sprintf("  rdfs:comment %s ;\n", $this->convertSchemaDescriptionToHtml($classCommentTerm->typeCoercedSerialize(new PrefixedIri('rdf', 'HTML'))));
        $output .= sprintf("  rdfs:label \"%s\" ;\n", $classLabelTerm->getRawValue());
        foreach ($properties as $property => $propertyData) {
            list(,$localPart) = explode(':', $property);
            $output .= sprintf("  sh:property %s-%s ;\n", $class, $localPart);
        }
        $output .= "  sh:closed true ;\n";
        $output .= "  sh:ignoredProperties (rdf:type rdfs:label) ;\n";
        $output .= ".\n";
        foreach ($properties as $property => $propertyData) {
            list(,$localPart) = explode(':', $property);
            $output .= sprintf("%s-%s\n", $class, $localPart);
            $output .= "  a sh:PropertyShape ;\n";
            $output .= sprintf("  sh:path %s ;\n", $property);
            $output .= sprintf("  sh:description %s ;\n", $propertyData['comment']);
            $output .= sprintf("  sh:name \"%s\" ;\n", $propertyData['label']);
            $output .= sprintf("  sh:or (\n");
            foreach ($propertyData['types'] as $type => $kind) {
                $output .= "      [\n";
                $output .= sprintf("        %s %s ;\n", $kind, $type);
                $output .= "      ]\n";
            }
            $output .= sprintf("  ) ;\n");
            $output .= ".\n";
        }
        // Save shacl output to a temp file and return the handle.
        $handle = fopen($this->destination, 'a');
        fwrite($handle, $output);
        fclose($handle);
    }

    // TODO: Improve convertion of schema.org descriptions. See https://github.com/schemaorg/schemaorg/pull/1075
    protected function convertSchemaDescriptionToHtml(string $description): string
    {
        preg_match_all('/\[\[(.+)]]/', $description, $matches, PREG_PATTERN_ORDER);
        if (isset($matches[0])) {
            foreach ($matches[0] as $key => $match) {
                $description = str_replace($match, sprintf('<a class=\"localLink\" href=\"%s%s\">', Constant::BASE_ADDRESS, $matches[1][$key]), $description);
            }
            $description = str_replace("\n", "<br/>\n", $description);
            $description = str_replace('\(s)', '(s)', $description);
        }
        $description = str_replace('\_', '_', $description);
        return $description;
    }
}
