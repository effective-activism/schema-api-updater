<?php declare(strict_types = 1);

namespace EffectiveActivism\SchemaApiUpdater\DependencyInjection;

use EffectiveActivism\SchemaApiUpdater\Queue\Handler;
use EffectiveActivism\SchemaApiUpdater\Service\Updater;
use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class SchemaApiUpdaterExtension extends Extension implements PrependExtensionInterface
{
    protected array $namespaces = [];

    /**
     * @throws Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $definition = $container->getDefinition(Updater::class);
        $definition->setArgument('$namespaces', $this->namespaces);
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $definition = $container->getDefinition(Updater::class);
        $definition->setArgument('$namespaces', $this->namespaces);
        $definition->setArgument('$destination', $config['shacl_file']);
        $definition = $container->getDefinition(Handler::class);
        $definition->setArgument('$destination', $config['shacl_file']);
    }

    public function prepend(ContainerBuilder $container)
    {
        // Get namespaces from the schema_api bundle.
        $config = $container->getExtensionConfig('schema_api');
        $this->namespaces = isset($config[0]['namespaces']) ? $config[0]['namespaces'] : [];
    }

    public function getAlias()
    {
        return 'schema_api_updater';
    }
}
