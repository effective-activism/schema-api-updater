<?php declare(strict_types = 1);

namespace EffectiveActivism\SchemaApiUpdater;

use EffectiveActivism\SchemaApiUpdater\DependencyInjection\SchemaApiUpdaterExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class EffectiveActivismSchemaApiUpdaterBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new SchemaApiUpdaterExtension();
    }
}
