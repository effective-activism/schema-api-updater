<?php declare(strict_types=1);

namespace EffectiveActivism\SchemaApiUpdater\Service;

use EffectiveActivism\SchemaApiUpdater\Queue\Item;
use EffectiveActivism\SparQlClient\Client\SparQlClientInterface;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Constraint\FilterNotExists;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Optionally\Optionally;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Triple\Triple;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\PrefixedIri;
use EffectiveActivism\SparQlClient\Syntax\Term\Path\ZeroOrMorePath;
use EffectiveActivism\SparQlClient\Syntax\Term\Variable\Variable;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Updater
{
    protected string $destination;

    protected HttpClientInterface $httpClient;

    protected MessageBusInterface $messageBus;

    protected array $namespaces = [];

    protected SparQlClientInterface $sparQlClient;

    public function __construct(string $destination, HttpClientInterface $httpClient, MessageBusInterface $messageBus, array $namespaces, SparQlClientInterface $sparQlClient)
    {
        $this->destination = $destination;
        $this->httpClient = $httpClient;
        $this->messageBus = $messageBus;
        $this->namespaces = $namespaces;
        $this->sparQlClient = $sparQlClient;
    }

    public function run()
    {
        // Get all classes.
        $classVariable = new Variable('class');
        $classesVariable = new Variable('classes');
        $commentVariable = new Variable('comment');
        $labelVariable = new Variable('label');
        $roleVariable = new Variable('role');
        $statement = $this->sparQlClient
            ->select([$classVariable, $commentVariable, $labelVariable])
            ->where([
                new Triple($classVariable, new PrefixedIri('rdf', 'type'), new PrefixedIri('rdfs', 'Class')),
                new Triple($classVariable, new PrefixedIri('rdfs', 'label'), $labelVariable),
                // Do not include data types.
                new FilterNotExists([
                    new Triple($classVariable, new PrefixedIri('rdf', 'type'), new PrefixedIri('schema', 'DataType')),
                ]),
                // Do not include derived data types.
                new FilterNotExists([
                    new Triple($classVariable, new ZeroOrMorePath(new PrefixedIri('rdfs', 'subClassOf')), $classesVariable),
                    new Triple($classesVariable, new PrefixedIri('rdf', 'type'), new PrefixedIri('schema', 'DataType')),
                ]),
                // Do not include roles.
                new FilterNotExists([
                    new Triple($roleVariable, new ZeroOrMorePath(new PrefixedIri('rdf', 'type')), new PrefixedIri('schema', 'Role')),
                    new Triple($classVariable, new ZeroOrMorePath(new PrefixedIri('rdfs', 'subClassOf')), $roleVariable),
                ]),
                new Optionally([
                    new Triple($classVariable, new PrefixedIri('rdfs', 'comment'), $commentVariable)
                ]),
            ]);
        $classSets = $this->sparQlClient->execute($statement);
        // TODO: Move file generation to own queue job. This will allow separation of concern.
        // Remove old file, if any.
        if (file_exists($this->destination)) {
            unlink($this->destination);
        }
        // Write prefixes.
        $output = '';
        $output .= "@prefix owl: <http://www.w3.org/2002/07/owl#> .\n";
        $output .= "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n";
        $output .= "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n";
        $output .= "@prefix sh: <http://www.w3.org/ns/shacl#> .\n";
        $output .= "@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .\n";
        foreach ($this->namespaces as $prefix => $url) {
            $output .= "@prefix $prefix: <$url> .\n";
        }
        $output .= "\n";
        $handle = fopen($this->destination, 'a');
        fwrite($handle, $output);
        fclose($handle);
        foreach ($classSets as $classSet) {
            $this->messageBus->dispatch(new Item($classSet));
        }
    }
}
