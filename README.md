# Schema API updater

A small service to update SHACL validation when the triplestore that powers
the Schema API is updated with new classes, properties or types.

## Installation

To install, run
```bash
composer require effectiveactivism/schema-api-updater
```

## Configuration

This bundle has no configuration but requires the
`effectiveactivism/sparql-client` bundle to work.

See [below](#asynchronous-handling) for information on setting up the
messaging system used to generate the SHACL file.

## Usage

After updating your triplestore, run this service to create a new SHACL
validation file. This file can be used by a validation service to ensure
that your validation is up-to-date with your triplestore classes.

```php
namespace App\Controller;

use EffectiveActivism\SchemaApiUpdater\Service\Updater;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MyController extends AbstractController
{
  public function example(Updater $updater)
  {
    $updater->run();
  }
}
```

Once generated, move the file from `/tmp/schema-api-updater-shacl.ttl`
(or wherever the system temporary directory is) to a destination visible
to the SHACL validator service.

## Asynchronous handling

This bundle uses the
[Symfony messaging system](https://symfony.com/doc/current/messenger.html)
.  
Because all classes, their properties and types are processed, it is
recommended to use an asynchronous queue to generate the SHACL file.

For more information, see
https://symfony.com/doc/current/messenger.html#transports-async-queued-messages
and
https://symfony.com/doc/current/messenger.html#consuming-messages-running-the-worker
